import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

/** El objetivo de vuex es una mejor gestión del estado de la aplicación. En este básico ejemplo
 * El estado del contador está definido en la vista Vuex.vue, el objetivo es sacar los datos que se
 * comparten a lo largo de la aplicación en varios componentes y centrarlizarlos en este file
 * llamado store.
 * 
 * Cuando los componentes se destruyen, se sigue manteniendo vivo el estado por la existencia de este plugin,
 * ejemplo: cuando se navega fuera de los componentes que tienen vuex, sin embargo se pierden cuando la
 * aplicación se recarga (para eso es el localstorage, o las cookies?)
 * 
 * Problema: Cuando se necesita hacer un cómputo sobre una propiedad del estado, (en el ejemplo, se 
 * multiplica el valor del counter por 2) esto no escala bien si se necesita hacer el cambio en 
 * diferentes lugares, entonces aquí entran los getters. Los getters son funciones que traen el dato
 * del store, y hacen cálculos con él antes de regresarlo al componente que lo requiere.
 */

export default new Vuex.Store({
  /** Aquí dentro se configurarán todas las propiedades necesarias para el state management.
   * En el ejemplo el counter del demo de vuex. Como práctica para mi, lo implementaré en el estado del drawer.
   * Veredicto, más rápido y fácil sin necesidad del uso del eventBus, con consistencia
   */
  state: {
    counter: 0,
    drawer: false,
    clicksCount: 0
  },
  getters: {
    /** Computed states? Algo similar. Los getters reciben como parámetro la propiedad state y tienen acceso
     * a sus propiedades internas para regresar el dato modificado (como un computed property, ya que 
     * aparentemente no hacen cambio directo a la propiedad del state)
     */
    doubleCounter: state => state.counter * 2,
    stringCounter: state => `${state.clicksCount} clicks`
  },
  /** Las mutations son algo similar a los getters, son los setters, las mutaciones son committed por los
   * diferentes componentes para evitar inconsistencias de todos los componentes que puedan mutar el estado.
   * Entonces, un componente hace un commit, y luego las mutations le hacen el cambio al estado.
   * (Quizá esto sea una manera más fácil de manejar la autenticación global en la aplicación ie. angular
   * auth change states con observables).
   * Las mutaciones son funciones que hacen cambios al estado. En este ejemplo se hace una centralización de
   * casi todo el código de la aplicación. Lo que me preocupa en algo mas grande es separation of concerns.
   * 
   * Veo que, aunque Max no lo haya explicado aún puedes agregar argumentos a los commits con payload, el cual
   * asumo que es un objeto js(de hecho puede ser cualquier variable, pero con objetos js se pueden pasar n
   * parámetros). Al final explica que se puede hacer paso de parámetros implícitamente aunque no estén declarados
   * en los mappers. Sin embargo, solo es posible pasar un parámetro, por lo tanto, para pasar n parámetros
   * se usan objetos js. Entonces haré DRY refactorizando estas funciones. tricky pero funcionó.
   * 
   * Las mutaciones deben ser siempre síncronas porque si no, pierden todo su proposito de trackar cual 
   * mutación hizo qué cambio en cuál estado.
   */
  mutations: {
    // increment: (state) => state.counter++,
    // decrement: state => state.counter--,
    changeCounter: (state, inc) => {
      // setTimeout(() => {}, 1000);
      inc ? state.counter++ : state.counter--;
      state.clicksCount++;

    }
  },
  /** Las acciones permiten la ejecución de código asíncrono que, una vez que hayan terminado sus tareas,
   * le pasan la información a las mutaciones para que se encarguen de hacer los commits al estado de la
   * aplicación. Independientemente de esto, es buena práctica utilizar úncamente acciones para modificar
   * estado en los componentes
   *
   */
  actions: {
    /** Se puede hacer destructuring para obtener el objeto commit si no se require algo más, porque
     * las acciones siempre recibirán el contexto como parámetro
     * Ej changeCounter: ({ commit }) => {...} 
     */
    asyncCounterChange: (context, payload) => {
      setTimeout(() => context.commit('changeCounter', payload.inc), payload.delay);
    }
  },
})

/** Separation of concerns: Lo que me preocupaba. Es posible separar todo el código que concierna a 
 * un solo segmento de la lógica del negocio separando los concerns en files distintos, aquí no hay razón
 * para hacer eso. Ej: En file counter.store.js (mi convención):
 * 
 * export default {
 *  state: { ... },
 *  mutations: { ... },
 *  actions: { ... },
 *  ...
 * } 
 * 
 * Supongo que se hacen merge con spread. Nota: siempre debe existir un store centralizado con un estado
 * central.
 * Pues no. Es más simple: Se importa el objeto y se agrega como otra propiedad del store centralizado llamada
 * modules.
 * 
 * modules: {
 *  counter
 * }
 * 
 * Tener cuidado ya que no deben existir dos getters o mutations o cualquier otra cosa nombrada de la
 * misma manera ya que, under the hood, todo esto hace merge en un solo store centralizado
 */