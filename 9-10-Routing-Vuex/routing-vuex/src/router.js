import Vue from 'vue'
import Router from 'vue-router'
// import User from './views/User'
import Home from './views/Home'

Vue.use(Router)

const router = new Router({
  /** Sin el mode history, el default es 'hash', example.com/#/something
   * Este modo funciona por el hecho de primero se necesita solicitar la spa al server
   * antes de que js pueda hacer uso de la ruta, entonces para el modo history se debe
   * configurar el server de manera que cualquier ruta redireccione el index.html, sin 
   * utilizar otro html, y permite la creación de rutas normales como 
   * example.com/something
   */
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    /** Si es que en el history hace un back para que no se vaya al hash, sino
     * al inicio de la página
     */
    if (savedPosition) {
      return savedPosition;
    }
    /** se revisa si la url contiene un hash */
    if (to.hash) {
      // console.log('hay hash');
      return {
        selector: to.hash
      };
    }
    return {
      x: 0,
      y: 0
    };
  },
  routes: [{
      /** Manera de hacer y recibir parámetros dinámicos */
      path: '/user',
      /** Viendo los docs, name funciona para hacer referencia a él desde sus llamadas, en lugar de
       * un string conteniendo el path, como un alias.
       * Max hace muy complejo el lazy loading, cuando en el ejemplo se hace de la siguiente sencilla
       * manera, será porque el curso ya está outdated y el router es mejor ahora?
       * https: //router.vuejs.org/guide/advanced/lazy-loading.html
       */
      component: () => import('./views/User'),
      /** Setting children routes, rutas anidadas que se cargaran en /user/child, el cuál dentro del
       * componente User se podrán cargar a través de otro router-view. Children es otro array de 
       * objertos js. El path de las routas puede utilizar un slash, lo que hará que la ruta sea
       * absoluta, es decir, si se le pone /algo, el algo será concatenado con la raíz omitiendo
       * el /user. lo cual cargaría el componente lógicamente dentro de User, aunque no lo refleje
       * en el path del navegador
       */
      children: [{
          path: '',
          component: () => import('./components/routing/UserStart'),
          name: 'user'
        },
        {
          path: ':id',
          component: () => import('./components/routing/UserDetail'),
        },
        {
          path: ':id/edit',
          component: () => import('./components/routing/UserEdit'),
          name: 'userEdit',
          /** También es posible utilizar una función para poner los guards, o simplemente un método
           * que se ejecuta antes de entrar, sin embargo también existe el hook dentro de las propiedades
           * del componente: Ver UserDetail
           */
          beforeEnter(to, from, next) {
            console.log('Inside route setup');
            next();
          }
        },
      ]
    }, {
      path: '',
      name: 'home',
      component: Home
    },
    {
      /** Redirección recibe un string para direccionar a otra ruta, también puede ser aplicado con un 
       * objeto js y prop name
       */
      path: '/redirect-me',
      // redirect: '/user'
      redirect: {
        name: 'user'
      }
    },
    /** Wild cards, * para atrapar cualquier ruta que no esté controlada con por las rutas que ya
     * están declaradas
     */
    {
      path: '/vuex',
      name: 'vuex',
      component: () => import('./views/Vuex')
    },
    {
      path: '*',
      redirect: '/'
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ],
})

/** Router hooks */
router.beforeEach((to, from, next) => {
  // console.log('global Before Each');
  next();
});

export default router;