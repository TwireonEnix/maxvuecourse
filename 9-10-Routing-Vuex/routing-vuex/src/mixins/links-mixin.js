export const links = {
  data: () => ({
    // Se quita al usar vuex
    // drawer: false,
    links: [{
        name: 'Home',
        route: '/',
        icon: 'home'
      },
      {
        name: 'User',
        route: '/user',
        icon: 'person'
      },
      {
        name: 'Vuex',
        route: '/vuex',
        icon: 'subway'
      }
      // {
      //   name: 'User',
      //   route: '/user/2',
      //   icon: 'person'
      // },
    ]
  })

};