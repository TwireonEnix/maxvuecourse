new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    specialsRemaining: 3,
    potions: 5,
    turns: [],
    color: 'red'
  },
  methods: {
    startGame: function () {
      this.gameIsRunning = true;
      this.playerHealth = 100;
      this.monsterHealth = 100;
      this.specialsRemaining = 3;
      this.potions = 5;
      this.turns = [];
    },
    attack: function () {
      let dmg = this.calculateDamage(10, 3);
      this.monsterHealth -= dmg
      this.writeLog(true, dmg, 'hit');
      dmg = this.calculateDamage(15, 6);
      this.playerHealth -= dmg
      this.writeLog(false, dmg, 'hit');
      this.checkWin();
    },
    specialAttack: function () {
      if (!this.specialsRemaining) return;
      let dmg = this.calculateDamage(20, 5);
      this.monsterHealth -= dmg;
      this.writeLog(true, dmg, 'hit with special attack');
      dmg = this.calculateDamage(15, 6);
      this.playerHealth -= dmg
      this.writeLog(false, dmg, 'hit');
      this.checkWin();
      this.specialsRemaining--;
    },
    heal: function () {
      if (!this.potions || this.playerHealth === 100) return;
      let heal = this.calculateDamage(15, 6);
      this.playerHealth += heal;
      if (this.playerHealth > 100) this.playerHealth = 100;
      this.writeLog(true, heal, 'healed for')
      const dmg = this.calculateDamage(10, 3);
      this.playerHealth -= dmg;
      this.writeLog(false, dmg, 'hit');
      this.checkWin();
      this.potions--;
    },
    giveUp: function () {
      this.playerHealth = 0;
      this.turns.unshift({
        isPlayer: true,
        text: 'surrendered'
      })
      this.checkWin();
    },
    calculateDamage: (min, max) => {
      return Math.floor(Math.random() * ((max + 1) - min) + min);
    },
    checkWin: function () {
      let message;
      if (this.monsterHealth <= 0) {
        message = 'won';
      } else if (this.playerHealth <= 0) {
        message = 'lost';
      }
      if (message) {
        confirm(`You ${message}! Do you want to play again?`) ? this.startGame() : this.gameIsRunning = false;
      }
    },
    writeLog: function (isPlayer, dmg, msg) {
      this.turns.unshift({
        isPlayer: isPlayer,
        text: `${msg} ${dmg} points of damage`
      })
    }
  }

});