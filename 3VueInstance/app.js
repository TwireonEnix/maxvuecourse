const vm1 = new Vue({
  data: {
    title: 'The VueJS Instance',
    showParagraph: false
  },
  methods: {
    show: function () {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function (title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function () {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function (value) {
      // alert('Title changed, new value: ' + value);
    }
  }
});
vm1.$mount('#app1');


const vm2 = new Vue({
  el: '#app2',
  data: {
    title: 'The second instance',
    showParagraph: false
  },
  beforeCreate() {
    console.log('beforeCreate')
  },
  created() {
    console.log('created');
  },
  beforeMount() {
    console.log('beforeMount');
  },
  mounted() {
    console.log('mounted');
  },
  beforeUpdate() {
    console.log('beforeUpdate');
  },
  updated() {
    console.log('updated');
  },
  beforeDestroy() {
    console.log('destroy');
  },
  destroyed() {
    console.log('destroyed');
  },
  methods: {
    show: function () {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function (title) {
      this.title = title;
    },
    onChange: function () {
      vm1.title = 'Changed with button'
      vm1.show();
    },
    destroyMe: function () {
      this.$destroy();
    }
  },
  computed: {
    lowercaseTitle: function () {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function (value) {
      // alert('Title changed, new value: ' + value);
    }
  }
});

setTimeout(() => {
  vm2.title = 'Changed by timer';
}, 3000);

console.log(vm1.$refs)

/** Similitud con react donde hay templates embebidos en código JS */
const vm3 = new Vue({
  template: '<h1>Hello</h1>'
})


/** manera de asignarle la instancia de vue a un elemento del html a través de 
 * vanilla js.
 */
vm3.$mount();
document.getElementById('app3').appendChild(vm3.$el);

// Vue.component('hello', {
//   template: '<h1>Hello</h1>'
// });