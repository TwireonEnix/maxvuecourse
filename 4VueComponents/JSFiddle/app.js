/** Versión componente */

const data = {
  status: 'Critical'
};


Vue.component('my-first-component', {
  data() {
    return {
      // status: 'Critical'
      /** Devolviendo únicamente la referencia no funcionaría ya que el mismo objeto sería compartido en todas
       * las instancias del componente, sin embargo es posible utilizar el operador spread para hacer una copia
       * del objeto que se asignará individualmente a cada componente distinto creado, el cuál tendrá su propio
       * elemento siendo reactivo a la vez
       */
      ...data
    }
  },
  methods: {
    fixServer: function () {
      /** Es posible acceder a la propiedad de status a través de this incluso cuando está encerrada en una función.
       * Vue hace esto posible behind the scenes
       */
      this.status = 'fixed'
    }
  },
  template: `<div>
      <p> Server status: {{ status }}</p>
      <button @click="fixServer">Change</button>
    </div>`
})


/** Instancia vue normal */
// new Vue({
//   data: {
//     status: 'Critical'
//   },
//   template: `<p> Server status: {{ status }}</p>`
// }).$mount('#app');

new Vue({}).$mount('#app');

/** Definición local de componentes */
const cmp = {
  data() {
    return { msg: 'Hello World ' }
  },
  methods: {
    addName() {
      this.msg += 'buddy'
    }
  },
  template: `<div> {{ msg}}
  <button @click="addName"> Add </button>
  </div>`
};

new Vue({
  components: {
    second: cmp
  }
}).$mount('#app2');