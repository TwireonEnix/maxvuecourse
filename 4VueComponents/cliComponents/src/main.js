import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';

export const ServerService = new Vue();

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount('#app');
