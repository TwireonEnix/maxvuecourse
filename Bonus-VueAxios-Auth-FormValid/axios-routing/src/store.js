import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios.auth';
import globalAxios from 'axios';
import router from './router';


Vue.use(Vuex)

/** Imporante notar que para apuntar a los mutators es con commit, y para los actions es con dispatch
 * a pesar de que haya métodos que tengan el mismo nombre
 */

export default new Vuex.Store({
  state: {
    /** Token de autenticación */
    idToken: null,
    userId: null,
    user: null
  },
  getters: {
    user(state) {
      return state.user;
    },
    isAuth(state) {
      return state.idToken !== null;
    }
  },
  mutations: {
    authUser(state, userData) {
      state.idToken = userData.idToken;
      state.userId = userData.userId;
    },
    storeUser(state, user) {
      state.user = user;
    },
    clearAuthData(state) {
      state.idToken = null;
      state.userId = null;
      state.user = null;
    }
  },
  actions: {
    setLogoutTimer(context, expirationTime) {
      setTimeout(() => {
        context.dispatch('logout')
        router.replace('/signin');
      }, expirationTime * 1000);
    },
    /** Quitar la lógica de signin y signup de los componentes y traerla aquí */
    signup(context, authData) {
      axios
        .post('signupNewUser?key=AIzaSyA1eJnrC6OYRBPNeexwrpruC7q8UT9hhXQ', {
          email: authData.email,
          password: authData.password,
          returnSecureToken: true
        })
        .then(res => {
          console.log(res);
          context.commit('authUser', {
            idToken: res.data.idToken,
            userId: res.data.localId
          });
          const now = new Date();
          /** Expires in regresa en miliseconds, entonces llamando a now.getTime()
           * igualmente convertirá el tiempo actual en milisegundos desde el epoch
           */
          const expirationDate = new Date(now.getTime() + res.data.expiresIn * 1000);
          localStorage.setItem('token', res.data.idToken);
          localStorage.setItem('userId', res.data.localId);
          localStorage.setItem('expirationDate', expirationDate);
          context.dispatch('storeUser', authData);
          context.dispatch('setLogoutTimer', res.data.expiresIn);
          /** Se guardan en distintos lugares, el auth y la bd, para guardar el usuario en la bd
           * de firebase se crea otra acción para eso, que usa el baseurl global de axios
           */
        })
        .catch(e => console.log(e));
    },
    tryAutoLogin(context) {
      const token = localStorage.getItem('token');
      if (!token) return;
      const expirationDate = localStorage.getItem('expirationDate');
      const now = new Date();
      const expiration = new Date(expirationDate);
      if (now >= expiration) return;
      const userId = localStorage.getItem('userId');
      context.commit('authUser', {
        idToken: token,
        userId
      })

    },
    login(context, authData) {
      axios
        .post('verifyPassword?key=AIzaSyA1eJnrC6OYRBPNeexwrpruC7q8UT9hhXQ', {
          email: authData.email,
          password: authData.password,
          returnSecureToken: true
        })
        .then(res => {
          console.log(res);
          context.commit('authUser', {
            idToken: res.data.idToken,
            userId: res.data.localId
          });
          const now = new Date();
          const expirationDate = new Date(now.getTime() + res.data.expiresIn * 1000);
          console.log(expirationDate);
          localStorage.setItem('token', res.data.idToken);
          localStorage.setItem('userId', res.data.localId);
          localStorage.setItem('expirationDate', expirationDate.toISOString());
          context.dispatch('fetchUser');
          context.dispatch('setLogoutTimer', res.data.expiresIn);
        })
        .catch(e => console.log(e));
    },
    logout(context) {
      context.commit('clearAuthData');
      localStorage.removeItem('expirationDate');
      localStorage.removeItem('userId');
      localStorage.removeItem('token');
      router.replace('/signin');
    },
    storeUser(context, userData) {
      if (!context.state.idToken) {
        return
      }
      globalAxios.post('users.json?auth=' + context.state.idToken, userData).then(res => console.log(res)).catch(e => console.log(e));
    },
    fetchUser(context) {
      globalAxios.get('users.json?auth=' + context.state.idToken)
        .then(res => {
          /** data regresa un objeto donde su llave para el usuario es el id que se le asigna
           * por lo tanto no se puede acceder directamente a él poniendo res.data.email.
           * Primero se extrae data en una variable, se crea un arreglo de usuarios y se
           * recorre el objeto con un forin loop, se obtiene el objeto con la key y se le
           * agrega la propiedad id para que sea su key. Después se envía al arreglo de
           * usuarios, el cuál ya permite acceder a sus propiedades
           */
          const {
            data
          } = res;
          const users = [];
          for (const key in data) {
            const user = data[key];
            user.id = key;
            users.push(user);
          }
          console.log(users);
          this.email = users[0].email;
          context.commit('storeUser', users[0]);
          router.replace('/dashboard');
        })
        .catch(e => console.log(e));
    }
  }
})