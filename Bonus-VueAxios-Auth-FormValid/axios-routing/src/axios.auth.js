import axios from 'axios';

/** Creación de otra instancia de axios para modularizar el uso. La instancia se puede 
 * configuar de la misma manera que la instancia global de axios
 */
const instance = axios.create({
  baseURL: `https://www.googleapis.com/identitytoolkit/v3/relyingparty/`
})

/** */
// instance.defaults.headers.common['SOMETHING'] = 'something';

export default instance;