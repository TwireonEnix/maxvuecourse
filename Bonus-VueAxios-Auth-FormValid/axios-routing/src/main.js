import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import router from './router'
import store from './store'
import vuelidate from 'vuelidate';

Vue.use(vuelidate);
/** Opciones globales (lista en los docs), la configración afecta a todas
 * las llamadas a axios a lo largo de la aplicación, se puede configurar de manera
 * global y local
 */
axios.defaults.baseURL = `https://max-project-ef1ba.firebaseio.com/`;
// axios.defaults.headers.common['Authorization'] = 'token de acceso';

/** Uso de interceptores. Los interceptores deben de regresar siempre su argumento
 * para no bloquear el proceso.
 */
axios.interceptors.request.use(config => {
  // console.log(`Request interceptor ${JSON.stringify(config, null, 2)}`);
  return config;
});

axios.interceptors.response.use(res => {
  // console.log(`Response interceptor ${JSON.stringify(res, null, 2)}`);
  return res;
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})