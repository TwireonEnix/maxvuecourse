import Vue from 'vue'
import VueRouter from 'vue-router'
/** se importa el store para tener acceso al estado de la aplicación y poder aplicar
 * los auth guards
 */
import store from './store';

Vue.use(VueRouter)

const routes = [{
    path: '/',
    component: () => import('./components/welcome/welcome')
  },
  {
    path: '/signup',
    component: () => import('./components/auth/signup')
  },
  {
    path: '/signin',
    name: 'login',
    component: () => import('./components/auth/signin')
  },
  {
    path: '/dashboard',
    component: () => import('./components/dashboard/dashboard'),
    beforeEnter(to, from, next) {
      if (store.state.idToken) {
        next()
      } else next('/signin')
    }
  }
]

export default new VueRouter({
  mode: 'history',
  routes
})