export const wordMixin = {
  computed: {
    countChars() {
      return `${this.message} (${this.message.length})`;
    }
  }
};