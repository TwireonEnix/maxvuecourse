/** Los mixins son piezas reutilizables de código que inteligentemente hacen merge con los datos
 * ya existentes del componente con el que se combinan, Los life hooks igualmente hacen merge, por lo
 * tanto, si en el mixin existen life hooks, así como en el componente que lo incorpora, ambos
 * se ejecutarán. Los life cycle se ejecutan primero dentro del mixin y después en el 
 * componente que los incorpora*/
export const fruitMixin = {
  data: () => ({
    fruits: ['Apple', 'Banana', 'Mango', 'Melon', 'Orange'],
    filterText: ''
  }),
  computed: {
    filteredFruits() {
      /** versión usando regex para que sea global e ignopre el case sensitive, habrá
       * performance impact en instanciar la expresión?
       */
      return this.fruits.filter(el =>
        el.match(new RegExp(this.filterText, 'gi'))
      );
    }
  },
  created() {
    console.log('Created in mixin');
  }
};