import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

/** Primera opción para crear directivas. La primera es dentro del mainjs y
 * registarlo de manera global en la app. Utilizando el método Vue.directive que recibe
 * los parámetros de: 1. nombre de la directiva: La cuál se utilizará utiliizando el 
 * prepend de todas, en este caso v-highlight, el v- es  necesario siempre para decirle 
 * a vue que se está trabajando con una directiva.
 * 2. El objeto que configura el comportamiento de la directiva:
 * 
 * Funcionamiento de las directivas
 * 
 * a) Hooks: Los hooks funcionan como los de life cycle, métodos que se ejecutan:
 * a.1) bind: Una vez que la directiva se ha bindeado a un elemento, recibe 3 argumentos
 * (el, binding, vnode) binding y vnode deben ser operados como readonly ya que influyen
 * en el funcionamiento interno de las directivas. binding se refiere a las funciones como 
 * modificadores adicionales que se pueden ejecutar sobre la directiva y el vnode representa
 * el elemento en el dom virtual.
 * 
 * inserted: (el, binding, vnode) Este ocurre cuando el elemento es insertado en el dom
 * update: (el, binding, vnode, oldVnode) cada vez que un componente es actualizado. (sin children)
 * componentUpdated(el, binding, vnode, oldVnode) Cuando el compomponente es actualizado (con children)
 * unbind(el, binding, vnode) Cuando el elemento se ha removido del dom
 * 
 * La segunda opción para la creación de directivas es dentro del script tag del componente que la
 * usará, lo que se llaman directivas locales
 */

Vue.directive('highlight', {
  bind(el, binding) {
    // el.style.backgroundColor = 'green';
    /** El binding.value se refiere a cuando se recibe algún parámetro en la directiva ej:
     * v-highlight="blue"
     */
    // el.style.backgroundColor = binding.value;
    /** En el siguiente caso se espera que la directiva reciba un parámetro adicional con
     * v-highlight:background, al que se puede accesar con binding.arg
     * 
     * El otro caso es que la directiva reciba un modificador, el cuál se declara con un dot
     * en este caso la manera de accesarlo en el template será
     * v-highlight.delay="red"
     */
    let delay = 0;
    if (binding.modifiers['delayed']) {
      delay = 2000;
    }
    setTimeout(() => {
      if (binding.arg === 'background') {
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }
    }, delay);

  }
});

/** Los filtros y mixins también se pueden declarar de manera global */
Vue.filter('toLower', value => value.toLowerCase());

/** Registro de un mixin (mixed in) global
 * NOTA: Por la manera en la que los mixins funcionan, los datos del mixin estarán disponibles en
 * toda la aplicación y harán merge con cada uno de los componentes instanciados de manera global,
 * por lo tanto, la documentación de Vue NO recomienda esta implementación. Usar con precaución,
 * porque realmente afecta globalmente a la aplicación. Los datos de los mixins son individuales
 * por componente, es decir, en el ejemplo existe una instancia de globalText por CADA componente
 * instanciado.
 */

/** Exercise 2 filter */
Vue.filter('countChars', value => `${value} (${value.length})`);

Vue.mixin({
  data: () => ({
    globalText: `Hey, I'm a global piece of data available everywhere`
  }),
  // created() {
  //   /** Esto se ejecuta como veintemil veces en la consola por todos los componentes que están
  //    * involucrados en la aplicación, y no sólo hablando de los creados por el devel, sino también
  //    * los de cualquier plugin que se esté incluyendo, como vuetify y router en este caso.
  //    */
  //   console.log('created from global');
  // }
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');