import Vue from 'vue';
import Router from 'vue-router';
import Directives from './views/Directives.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'directives',
    component: Directives
  }, {
    path: '/exd',
    name: 'exd',
    component: () => import('./views/ExersiceDirectives')
  }, {
    path: '/filter',
    name: 'filter',
    component: () => import('./views/Filters')
  }, {
    path: '/exfm',
    name: 'exfm',
    component: () => import('./views/ExersiceFilterMixin')
  }, {
    path: '/anim',
    name: 'anim',
    component: () => import('./views/Animations')
  }]
});